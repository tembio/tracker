var express = require('express');
var router = express.Router();
var db = require('../lib/database.js');
var authHelper = require('../lib/authHelper.js');
var avertMailer = require('../lib/mailer.js');
var encrypt = require('../lib/encrypt.js');
var fs = require('fs');
var hogan = require("hogan.js");


router.route('/')
	/*Get user info*/
	.get(
		authHelper.ensureAuthorized,authHelper.getUserID,function(request,response){

			var success = function(user){
				response.json({'id':parseInt(request.userID),'email':user.email,
									'status':user.status,'lastTested':user.lastTested});
			};
			var fail = function(){
				response.status(404).send('No user found in DB');
			}

			db.getUserById(request.userID,success,fail);
		}
	)
	/*Create new user*/
	.post(
		function(request,response){

		  var success = function(user){
			response.status(201).json(user);
		  };
		  var fail = function(){
			response.status(409).send('User already exists');
		  }

		  var defaultUser = {'password':  encrypt.getInstance().hash(request.body.password),'email':request.body.email,'status':'neg','lastTested':''} 

		  db.createNewUser(defaultUser,success,fail);
		}
	)
	/*Update user info*/
	.patch(
		authHelper.ensureAuthorized,authHelper.getUserID,function(request,response){

			var success = function(updatedUser){

			  if(updatedUser.status == 'pos'){
				notifyContacts(updatedUser, response);
				return;
			  }else
			    response.status(204).send('User updated correctly');
			};

			var fail = function(code, msg){
			  response.status(code).send(msg);
			};

			if(!request.body.password){
				db.updateUser(request.userID,request.body,success,fail);
			}else{
				db.getUserById(request.userID,
					function(user){
						if(encrypt.getInstance().compare(request.body.oldPassword,user.password)){
							request.body.password = encrypt.getInstance().hash(request.body.password);
							db.updateUser(request.userID,request.body,success,fail);
						}else{
							fail(401,"Current password is incorrect");	
						}
					}, 
					function(){}
				);
			}

	});

router.route('/newcontact')
	/*Add new contact to the user's contacts list*/
	.post(authHelper.ensureAuthorized,authHelper.getUserID,function(request,response){

		var success = function(){
			db.addContact(request.body.newContactID,request.userID,
				function(){response.status(200).send('Contact added correctly');},
				fail);
		}
		var fail = function(code,msg){
			response.status(code).send(msg);
		}

		if(!request.body.newContactID){
			return response.status(400).send('No new user info sent in body');
		}else{
			db.addContact(request.userID,request.body.newContactID,success,fail);
		}

	});

var notifyContacts = function(positiveUser, response){

	var fail = function(){
		throw("Error getting contacts!");
	};

	var success = function(contactsList){		

		var contactsAfterTest = getContactsPosteriorToLastTest(positiveUser, contactsList);

		for(var i=0;i<contactsAfterTest.length;i++){

		  var contactId = contactsAfterTest[i];

		  db.getUserById(contactId,
		  	function(contact){

				  	  fs.readFile('views/positiveContactEmail.html', 'utf8', function (err,notificationEmail) {
				  	  	if (err){ 
						 	response.sendStatus(500);
					  	}else{
							avertMailer.sendHTMLEmail(contact.email, text.notificationEmailTitle, 
													  notificationEmail,
													  text.notificationEmailTextBody);	
			  				response.status(204).send('User updated correctly');
					    }
				  	  });
		  	},
		  	function(){/*User deleted don't send enything, dismiss*/}
		  );

		}
	};

	db.getContacts(positiveUser.id, success, fail);
}

/*The same email could appear several times but with different dates, if the user have met the same person more than once,
	so we have to make sure we compare only the last date of each user
*/
var getContactsPosteriorToLastTest = function(user, lastTestedList){
	var finalContacts = {},  /*{'email':date}*/
		userLastTestDate = user.lastTested;

	if(userLastTestDate == '')
		userLastTestDate = -1;

	for(var i=0;i<lastTestedList.length;i++){
		var contact = lastTestedList[i];

		if(contact.date >= userLastTestDate)
			if(contact.id in finalContacts){
				if(contact.date > finalContacts[contact.id])
					finalContacts[contact.id] = contact.date;
			}else{				
					finalContacts[contact.id] = contact.date;
			}
	}

	return Object.getOwnPropertyNames(finalContacts);
}

router.route('/delete')
	.post(authHelper.ensureAuthorized,authHelper.getUserID,function(request,response){

		var success = function(){
			response.status(200).send('User deleted');
		}
		var fail = function(code,msg){
			response.status(code).send(msg);
		}

		if(!request.body.id)
			return response.status(400).send('No new user info sent in body');
		else
			db.deleteUser(request.body.id,success,fail);
		

	});


//Exports to test
module.exports = router;