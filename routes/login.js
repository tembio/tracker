var express = require('express');
var router = express.Router();
var db = require('../lib/database.js');
var authHelper = require('../lib/authHelper.js');
var avertMailer = require('../lib/mailer.js');
var encrypt = require('../lib/encrypt.js');
var fs = require('fs');
var hogan = require("hogan.js");
var text = require("../text.js")


router.route('/')
	.post(
		function(request,response){
			var fail = function(){
				response.status(404).send('No user found');
			};
			var success = function(user){

				if(!encrypt.getInstance().compare(request.body.password, user.password)){
					response.status(401).send('Incorrect password');
				}else{
					response.header('authorization',authHelper.createToken(user.email));
					var userInfo = {id:user.id,status:user.status,lastTested:user.lastTested};
					response.status(200).json(userInfo);
				}
			};

			db.getUserByEmail(request.body.email, success, fail);
		}
	);


router.route('/recover')
	.post(
		function(request,response){
			var email = request.body.email;
			db.getUserByEmail(email,
				function(user){
					var newPassword = generateRandomPassword(10);
					var newPasswordHashed = encrypt.getInstance().hash(newPassword);

					db.updateUser(user.id,{password:newPasswordHashed},
						function(){

						  	  fs.readFile('views/passwordRecovery.html', 'utf8', function (err,newPassEmail) {
						  	  	if (err){ 
								 	response.sendStatus(500);
							  	}else{

									avertMailer.sendHTMLEmail([email], text.recoveryEmailTitle, 
															  hogan.compile(newPassEmail).render({'newPassword': newPassword}),
															  text.recoveryEmailTextBody + newPassword);	
									response.sendStatus(200);
							    }
						  	  });

						},function(){
							response.sendStatus(404);
					});
					
				},
				function(){response.sendStatus(404);});
		}	
	);


function generateRandomPassword(length){
	var characters = "abcdefghijklmnopqrst!$1234567890";
	var password = "";

	length = length||10;

	for(var index=0;index<length;index++)
		password += characters[Math.floor(Math.random()*characters.length)];

	return password;
}


module.exports = router;