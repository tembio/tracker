//String text
var messageUnsetDate = "Not tested yet";



//Interface
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function deleteCookie( cname ) {
  document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function createQR(){
	new QRCode($("#QR")[0], {
	    text: JSON.parse(getCookie("user"))["id"],
	    width: 128,
	    height: 128,
	    colorDark : "black",
	    colorLight : "white",
	    correctLevel : QRCode.CorrectLevel.H
	});
	$("#QR")[0].title = "QR-Code";
}

function populateUserData(){
    //$("#datepicker").datepicker();
	$('#datepicker').datepicker({
	    format: 'mm/dd/yyyy'
	});

	var testDate = formatTimeStamp(JSON.parse(getCookie("user"))["lastTested"]);

	if(testDate == "NaN/NaN/NaN" || testDate == "01/01/1970"){
		$('#datepicker input').val(messageUnsetDate);	
	}else{
		$('#datepicker input').val(testDate);
	}

	if(JSON.parse(getCookie("user"))["status"]=="neg")
	  $("#status")[0].selectedIndex = 0;
	else
	  $("#status")[0].selectedIndex = 1;

}






document.onreadystatechange = function(e)
{
    if (document.readyState === 'interactive')
    {
		if(getCookie("token")=="")
			window.location.replace("/");
    }
};


$( document).ready(function(){
	createQR();
	populateUserData();
});





$(".navbar li").click(function(){
	menuOptions = $(".navbar li");
	menuOptions.removeClass("active");
	$(this).addClass("active");
});

$("#navbar a").click(function () {
         $("#navbar").collapse('hide');
});

$("#menuSelectedHome").click(function(){
	$(".menu").hide();
	$("#home").show();
});
$("#menuSelectedQR").click(function(){
	$(".menu").hide();
	$("#QRMenu").show();
});
$("#menuSelectedStatus").click(function(){
	$(".menu").hide();
	$("#statusMenu").show();
});



$("#changeStatusButton").click(function(){
	  var newTestedDate = getTimeStamp($('#datepicker input').val());
	  var newStatus = $("#status").val();
	  var notTested = $('#datepicker input').val() == messageUnsetDate;

	  if( !notTested && newTestedDate==-1){
	  	alert("Wrong date, please pick a date from the calendar, or leave this field empty.");
	    return;
	  }

	  if(newStatus=="pos")
		if (confirm("Are you sure you want to set your status to positive?") == false)
			return;

	  $.ajax({
	      type: "PATCH",
	      beforeSend: function (request)
          {
            request.setRequestHeader("authorization", getCookie("token"));
          },
	      url: "users",
	      data: {status:newStatus,lastTested:newTestedDate},
	      success: function(data,status,xhr){
	      	var message="Changes saved!";
	      	if(newStatus=="pos")
	      		message = "\nWe have sent anonymous messages to the people you have been with asking them to get tested, thanks for helping!";
	      	alert(message);
	      	return;
	      },
	      error: function(XMLHttpRequest, textStatus, errorThrown) {
	         alert("There was an error updating your data, please try again.")
	      }
	  });

});

$("#deleteAccountButton").click(function(){

	if (confirm("Are you sure you want to delete your account?") == false)
		return

	$.ajax({
	      type: "POST",
	      beforeSend: function (request)
          {
            request.setRequestHeader("authorization", getCookie("token"));
          },
	      url: "users/delete",
	      data: {id:JSON.parse(getCookie("user"))["id"]},
	      complete: function(){
	      	deleteCookie("user");
	      	deleteCookie("token");
	      	window.location.replace("/");
	      }
	  });
});

$("#changePassButton").click(function(){
	  var newPass = $("#newPass").val();
	  var currentPass = $("#currentPass").val();
	  var newPassConfirm = $("#newPassConfirm").val();

	  if(newPass=="" || newPassConfirm=="" || currentPass==""){
	  	alert("All fields are required to change the password.");
	  	return;
	  }
	  
	  if(newPass!=newPassConfirm){
	  	alert("The confirmation password is different.");
	  	return;
	  }

	  $.ajax({
	      type: "PATCH",
	      beforeSend: function (request)
          {
            request.setRequestHeader("authorization", getCookie("token"));
          },
	      url: "users",
	      data: {password:newPass,oldPassword:currentPass},
	      success: function(data,status,xhr){
	      	var message="Changes saved!";
	      	alert(message);
	      	return;
	      },
	      error: function(XMLHttpRequest, textStatus, errorThrown) {
	         alert("The current password is incorrect!")
	      },
	      complete: function(){
	      	$("#newPass").val("");
	  		$("#currentPass").val("");
	  		$("#newPassConfirm").val("");
	      }
	  });

});


/*
* Returns timestamp if date is correct
* Returns -1 if date is incorrect
*/
function getTimeStamp(date){
	  if(date=="" || date==messageUnsetDate)
	  	return -1;

	  var dateFormat = /\d\d\/\d\d\/\d{4}/;
  	  if(!dateFormat.test(date)){
  	      return -1;
  	  }else{
		  return (new Date(date)).getTime();  	  	
  	  }
}

function formatTimeStamp(timestamp){
	var date = new Date(parseInt(timestamp));
    var dd = date.getDate();
    var mm = date.getMonth()+1; 
    var yyyy = date.getFullYear();

    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    return mm+'/'+dd+'/'+yyyy;
}

$("#menuSelectedSettings").click(function(){
	$(".menu").hide();
	$("#settingsMenu").show();
});

//QR read

sendContactId = function(data){
	$.ajax({
	      type: "POST",
	      beforeSend: function (request)
          {
            request.setRequestHeader("authorization", getCookie("token"));
          },
	      url: "newcontact",
	      data: {newContactID:data},
	      success: function(data,status,xhr){
	      	alert("Error saving contact");
	      	$("#loading").addClass("invisible");
	      },
	      error: function(XMLHttpRequest, textStatus, errorThrown) {
	      		alert("Contact saved!");
	      		$("#loading").addClass("invisible");
	      }
	});
	$("#loading").addClass("invisible");
};

function readURL(input) {

	var imgElem = new Image();
	imgElem.width = 128;
	imgElem.height = 128;
	imgElem.src = URL.createObjectURL(input.files[0]);

	QCodeDecoder().decodeFromImage(imgElem,function(er,res){
		if(er){
	 		var errMsg = "Please try again making sure the image is centered and focused.";
			alert(errMsg);
			$("#loading").addClass("invisible");	
	 	}else{
	 		addContact(res);
	 	}
	});
}

$("#inputImage").change(function(){
	$("#loading").removeClass("invisible");
    readURL(this);
    this.value="";
});

$("#loading").click(function(){
	$("#loading").addClass("invisible");
});

//Log out
$("#logOut").click(function(){
	logOut();	
});

function logOut(){
 	deleteCookie("user");
 	deleteCookie("token");
	window.location.replace("/");
}

function addContact(id){
	$.ajax({
      type: "POST",
      beforeSend: function (request)
      {
        request.setRequestHeader("authorization", getCookie("token"));
      },
      url: "users/newcontact",
      data: {newContactID:id},
      success: function(data,status,xhr){
      	var messageContactAdded = "Done!";
      	alert(messageContactAdded);
      	return;
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
      	 if(XMLHttpRequest.statusCode()=='404')
           alert("Invalid user QR")
         else
           alert("Coudln't send QR code, please try again.")
      },
      complete: function(){
		$("#loading").addClass("invisible");	
      }
  	});
}





