var request = require('supertest');
var assert = require('assert');
var sinon = require('sinon');
var app = require('../app');
var authHelper = require('../lib/authHelper.js');
var db = require('../lib/database.js');
var users = require('../routes/users.js');
var moment = require('moment'); 
var encrypt = require('../lib/encrypt.js');
var avertMailer = require('../lib/mailer.js');
    
var encryptedPass= encrypt.getInstance().hash('pwd');
var fakeUser  = {'password':encryptedPass,'email':'a@a.com','status':'false','lastTested':'20160813'};
var testToken = authHelper.createToken(fakeUser.email);


function beforeEachActions(done){

    db.flushDB();
    var success = function(user){done()};
    var fail = function(){done()};
    db.createNewUser(fakeUser,success,fail);
}

describe('Request user info', function() {

  beforeEach(function (done) {
    beforeEachActions(done);
  });

  it('Returns a 401 status code if user do not exist or not authenticated', function(done) {

    request(app)
      .get('/users')
      .set('authorization',authHelper.createToken("NonExistantEmail"))
      .expect(401, done);

  });

  it('Returns a 200 status code if found', function(done) {

    request(app)
      .get('/users')
      .set('authorization',testToken)
      .expect(200, done);

  });

  it('Returns a JSON format', function(done) {

    request(app)
      .get('/users')
      .set('authorization',testToken)
      .expect('Content-Type', /json/, done);

  });

 it('Returns correct user ID', function(done) {

    request(app)
      .get('/users')
      .set('authorization',testToken)
      .end(function(err,res){
        assert.equal(res.body.id,1);
        done();
      });
  });

  it('Returns user email field', function(done) {

    request(app)
      .get('/users')
      .set('authorization',testToken)
      .end(function(err,res){
        assert.equal(res.body.email,'a@a.com');
        done();
      });
  });

  it('Returns user status field', function(done) {

    request(app)
      .get('/users')
      .set('authorization',testToken)
      .end(function(err,res){
        assert.equal(res.body.status,'false');
        done();
      });
  });

  it('Returns user lastTested field', function(done) {

    request(app)
      .get('/users')
      .set('authorization',testToken)
      .end(function(err,res){
        assert.equal(res.body.lastTested,'20160813');
        done();
      });
  });

});

describe('Create new user',function(){

  beforeEach(function (done) {
    beforeEachActions(done);
  });

  it('Returns 409 status code if user exists', function(done){
    request(app)
             .post('/users')
             .send('email=a@a.com&password=pwd')
             .expect(409,done);
  });

  it('Returns 201 status code if user is created', function(done){
    request(app)
             .post('/users')
             .send('email=pepe@gmail.com&password=pwd')
             .expect(201,done);
  });

  it('Returns JSON', function(done){
    request(app)
              .post('/users')
              .send('email=pepe@gmail.com&password=pwd')
              .expect('Content-Type',/json/,done);
  });

  it('Returns user ID', function(done){
    request(app)
              .post('/users')
              .send('email=pepe@gmail.com&password=pwd')
              .end(function(err,res){
                assert(res.body.id);
                done();
              });
  });
  
});

describe('Update user info ',function(){

  var stubSendHTMLEmail;

  beforeEach(function (done) {
    beforeEachActions(done);
  });

  afterEach(function(done){
    if(stubSendHTMLEmail) stubSendHTMLEmail.restore();
    done();
  });

  it('Returns 204 status code if updated sucessfully',function(done){
    var updates = 'lastTested=20160815';
    request(app)
            .patch('/users')
            .set('authorization',testToken)
            .send(updates)
            .expect(204,done);
  });

  it('Returns 401 status code if user does not exist',function(done){

    var updates = 'lastTested=20160815';

    request(app)
            .patch('/users')
            .set('authorization',authHelper.createToken("NonExistantEmail"))
            .send(updates)
            .expect(401,done);
  });

  it('Changed fields are modified in user',function(done){
    var updates = 'password=pwdChanged&oldPassword=pwd&status=StatusChanged&lastTested=MonthChanged';
    request(app)
            .patch('/users')
            .set('authorization',testToken)
            .send(updates)
            .end(function(err,res){
              db.getUserById('1',function(user){
                            assert.equal(user.password,encrypt.getInstance().hash('pwdChanged'));
                            assert.equal(user.status,'StatusChanged');
                            assert.equal(user.lastTested,'MonthChanged');
                            done();
                          },
                          function(){done();});
            });
  });

  it('401 is return if current password needed to modify pwd is not correct',function(done){
    var updates = 'password=pwdChanged&oldPassword=blabla';
    request(app)
            .patch('/users')
            .set('authorization',testToken)
            .send(updates)
            .expect(401,done);
  });

  it('Unchanged fields stay the same in user',function(done){
    var updates = 'password=pwdChanged&oldPassword=pwd';
    request(app)
            .patch('/users')
            .set('authorization',testToken)
            .send(updates)
            .end(function(err,res){
              db.getUserById('1',function(user){
                            assert.equal(user.email,'a@a.com');
                            assert.equal(user.status,'false');
                            assert.equal(user.lastTested,'20160813');
                            done();
                          },
                          function(){done();});
            });
  });

  it('Contacts are notified when status is changed to positive (true)',function(done){    
    done();
/*
    stubSendHTMLEmail = sinon.stub(avertMailer,"sendHTMLEmail",function(){});

    var updates = 'status=true';
    var secondUser  = {'password':encrypt.getInstance().hash('pwd'),'email':'a2@a.com','status':'false','lastTested':'20160813'}

    db.createNewUser(secondUser,
                      function(newUser){
                        db.addContact(1,2, function(){

                                request(app)
                                  .patch('/users')
                                  .set('authorization',testToken)
                                  .send(updates)
                                  .end(function(err,res){

                                      done();

                                  });

                        }, function(){throw("Error adding contact")});
                      },
                      function(){ 
                        throw("Error creating second user")
                      });
*/
  });

  it('Contacts are notified if encounter date was after last tested date',function(done){    
    done();
    });

  it('Contacts are NOT notified if encounter date was after last tested date',function(done){    
    done();
  });

  it('Contacts are notified only once even if the have been with the user more times',function(done){    
    done();
  });


});




describe('Delete account',function(){

  beforeEach(function (done) {
    beforeEachActions(done);
  });

  it('Returns 200 status code if user is deleted',function(done){
    request(app)
            .post('/users/delete')
            .set('authorization',testToken)
            .send('id=1')
            .expect(200, done);
  });

  it('Returns 400 status code if user is no user is sent',function(done){
    request(app)
            .post('/users/delete')
            .set('authorization',testToken)
            .expect(400, done);
  });

  it('Returns 404 status code if user is not found',function(done){
    request(app)
            .post('/users/delete')
            .set('authorization',testToken)
            .send('id=wrong')
            .expect(404, done);
  });

  it('Removes user form DB',function(done){
    request(app)
            .post('/users/delete')
            .set('authorization',testToken)
            .send('id=1')
            .end(
              function(err,res){
                db.getUserByEmail(fakeUser.email, 
                  function(user){done(err);}, 
                  function(){done();}
                );
              }
            );
  });

});

describe('Add new contact',function(){

  var testToken;
  var secondUser;

  beforeEach(function (done) {
    var user  = {'password':encrypt.getInstance().hash('pwd'),'email':'a2@a.com','status':'false','lastTested':'20160813'}
    testToken = authHelper.createToken(fakeUser.email);

    db.flushDB();

    db.createNewUser(fakeUser,function(firstUser){
          db.createNewUser(user,
                      function(newUser){
                        secondUser = newUser;
                        done();
                      },
                      function(){ 
                        throw("Error creating second user")
                      });
    },function(){});

  });

  it('Returns 200 status code when user found',function(done){
    request(app)
            .post('/users/newcontact')
            .set('authorization',testToken)
            .send('newContactID='+secondUser.id)
            .expect(200, done);
  });

  it('Returns 401 when user is not found',function(done){
      request(app)
              .post('/users/newcontact')
              .set('authorization',authHelper.createToken('wrongEmail'))
              .send('newContactID='+secondUser.id)
              .expect(401, done);
  });

  it('Returns 400 when new contact is not being sent',function(done){
      request(app)
              .post('/users/newcontact')
              .set('authorization',testToken)
              .send('noContactInfo=blabla')
              .expect(400, done);
  });

    it('Returns 404 when new contact to add is not on DB',function(done){
      request(app)
              .post('/users/newcontact')
              .set('authorization',testToken)
              .send('newContactID=notValid')
              .expect(404, done);
  });

  it('Returns 200 when new contact is added',function(done){
      request(app)
              .post('/users/newcontact')
              .set('authorization',testToken)
              .send('newContactID='+secondUser.id)
              .expect(200, done);
  });

  it('New contact added in list of contacts of the person who scanned the QR',function(done){
      var expected = '{"id":'+secondUser.id+',"date":'+moment().format("YYYYMMDD")+'}';

      request(app)
              .post('/users/newcontact')
              .set('authorization',testToken)
              .send('newContactID='+secondUser.id)
              .end(function(err,res){

                db.getContacts('1',
                            function(contacts){
                              assert.equal("2",contacts[0].id);
                              done();
                            },
                            function(){});
              });
  });


  it('New contact added in list of contacts of the people whose QR was scanned',function(done){
      var expected = '{"id":'+secondUser.id+',"date":'+moment().format("YYYYMMDD")+'}';

      request(app)
              .post('/users/newcontact')
              .set('authorization',testToken)
              .send('newContactID='+secondUser.id)
              .end(function(err,res){

                db.getContacts('2',
                            function(contacts){
                              assert.equal("1",contacts[0].id);
                              done();
                            },
                            function(){});
              });
  });


  it('New contact not added in last date of same contact is same day',function(done){
      //This works thanks to the restrictions of the sets (no repeated elements)
      var expected = '{"id":'+secondUser.id+',"date":'+moment().format("YYYYMMDD")+'}';

      request(app)
              .post('/users/newcontact')
              .set('authorization',testToken)
              .send('newContactID='+secondUser.id)
              .end(function(err,res){
                  request(app)
                    .post('/users/newcontact')
                    .set('authorization',testToken)
                    .send('newContactID='+secondUser.id)
                    .end(function(err,res){

                      db.getContacts('1',
                                function(contacts){
                                  assert.equal(1,contacts.length);
                                  done();
                                },
                                function(){});
                    });
              });
  });

});


