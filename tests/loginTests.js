var request = require('supertest');
var assert = require('assert');
var app = require('../app');
var authHelper = require('../lib/authHelper.js');
var db = require('../lib/database.js');
var sinon = require('sinon');
var avertMailer = require('../lib/mailer.js');
var encrypt = require('../lib/encrypt.js');


function beforeEachActions(done){
    fakeUser  = {'password':encrypt.getInstance().hash('pwd'),'email':'a@a.com','status':'true','lastTested':'april'}
    db.flushDB();
    var success = function(user){done()};
    var fail = function(){done()};
    db.createNewUser(fakeUser,success,fail);
}

describe('Log in user',function(){
    
  beforeEach(function (done) {
    beforeEachActions(done);
  });
  
  it('Returns 200 status code when user logs in',function(done){
      request(app)
              .post('/login')
              .send('email=a@a.com&password=pwd')
              .expect(200, done);
    });

  it('Returns 401 when user is not found',function(done){
      request(app)
              .post('/login')
              .send('email=wrongEmail&password=pwd')
              .expect(404, done);
    });

  it('Returns 401 when password is incorrect',function(done){
    request(app)
            .post('/login')
            .send('email=a@a.com&password=WrognPass')
            .expect(401, done);
  });

  it('Returns JSON when user logs in',function(done){
      request(app)
              .post('/login')
              .send('email=a@a.com&password=pwd')
              .expect('Content-Type', /json/, done);
  });

  it('Returns token when user logs in',function(done){
      request(app)
              .post('/login')
              .send('email=a@a.com&password=pwd')
              .end(function(err,res){
                  assert.notEqual(undefined,res.headers["authorization"]);
                  done();
            });
  });

});

describe('Recover password',function(){
  var stub,spyDB;

  beforeEach(function (done) {
    stub = sinon.stub(avertMailer,"sendHTMLEmail", function(){});
    spyDB = sinon.spy(db,"updateUser");

    beforeEachActions(done);
  });

  afterEach(function(done){
    stub.restore();
    if(spyDB) spyDB.restore();
    done();
  });

  it('Recover new password returns 200 when successful',function(done){
      request(app)
              .post('/login/recover')
              .send('email=a@a.com')
              .expect(200,done);

  });

  it('Recover new password returns 404 when email not found',function(done){
      request(app)
              .post('/login/recover')
              .send('email=no@a.com')
              .expect(404,done);
  });

  it('Saves new random password is called',function(done){

      request(app)
              .post('/login/recover')
              .send('email=a@a.com')
              .end(function(err,res){
                  assert(spyDB.called);
                  done();
            });
      
  });

  it('Recover new password calls avertmailer when email is found',function(done){
      request(app)
              .post('/login/recover')
              .send('email=a@a.com')
              .end(function(err,res){
                  assert(stub.called);
                  done();
            });
  });

});
