var bcrypt = require('bcrypt');

module.exports  = (function () {
    var instance;
 
    function createInstance() {
        var s = bcrypt.genSaltSync(10);
        return {salt: s,
                compare: function(notHashed,hashed){return bcrypt.compareSync(notHashed, hashed);},
                hash: function(value){return bcrypt.hashSync(value,this.salt);}
        };
    }
 
    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();