var jwt    = require('jsonwebtoken');
var db = require('./database.js');
var config = require('../config.txt');  
var moment = require('moment');  

var secret = config.jwtSecret; 

module.exports = {

	createToken:function(user){
	  var payload = {
	      sub: user,
	      iat: moment(),
	      exp: moment().add(config.tokenExpirationTimeMinutes, "minutes"),
	    };
	  return token = jwt.sign(payload, secret);
	},

	getUserID:function(req, res, next){
		if(req.email == undefined)
		{
			res.sendStatus(401);
		}

		db.getUserId(req.email,
					function(userId){
						req.userID = userId;
						next();
					},
					function(){
						res.status(401).send('Error getting user');
					});
	},

	ensureAuthorized:function(req, res, next) {
		var bearerToken = req.headers["authorization"];

	    if (typeof bearerToken !== 'undefined') {
			var payload = jwt.decode(bearerToken, secret); 

			if(payload.exp <= moment()) {
			     return res
			        .sendStatus(401)
			        .json({message: "Expired token"});
			}
			req.email = payload.sub;
	        next();
	    } else {
	        res.redirect('/');
	    }
	}

};

