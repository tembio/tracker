var request = require('supertest');
var assert = require('assert');
var sinon = require('sinon');
var app = require('../app');
var avertMailer = require('../lib/mailer.js');
var db = require('../lib/database.js');
var encrypt = require('../lib/encrypt.js');

var fakeUser  = {'password':encrypt.getInstance().hash('pwd'),'email':'fake@a.com','status':'true','lastTested':'april'};

describe('Sign up new user', function() {

  beforeEach(function (done) {
    db.createNewUser(fakeUser, function(u){done();}, function(){done();});
  });

  afterEach(function(done){
    db.flushDB();
    done();
  });

  it('Returns a 200 status code if no problems', function(done) {
    request(app)
      .post('/signup')
      .send('email=new@a.com&password=pwd')
      .expect(200);
      done();
  });

  it('Returns a 400 status code if email is incorrect', function(done) {
    request(app)
      .post('/signup')
      .send('email=@a.com&password=pwd')
      .expect(400);
      done();
  });

  it('Returns a 409 status code if user already exists', function(done) {

    db.createNewUser(fakeUser,
        function(u){
          request(app)
              .post('/signup')
              .send('email=fake@a.com&password=pwd')
              .expect(409, done);
        },
        function(){done();}
    );
 
  });

});


describe('Send account confirmation', function() {

  beforeEach(function(done){    

    db.flushDB();
    db.setTempVariable("fake@a.com",1,
      function(){
        db.createNewUser(fakeUser,
          function(u){},
          function(){});
        },
      function(){});

    done();
  });

  afterEach(function(done){
    db.flushDB();
    done();
  });

  it('Returns a 200 status code', function(done) {    
    request(app)
      .get('/signup/confirm?e=fake@a.com')
      .expect(200,done);
  });

  it('Returns html', function(done) {    
    request(app)
      .get('/signup/confirm?e=fake@a.com')
      .end(function(err,res){
        assert(res.header['content-type'] == 'text/html; charset=utf-8');
        done();
      });
  });

});




