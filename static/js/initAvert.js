
$( document).ready(function(){
  $("#logo").hide();
  $("#avertMail").hide();
  $(".row-fluid").hide();
  $("#logo").fadeIn(1000);
  $(".row-fluid").fadeIn(2500);
  $("#avertMail").fadeIn(3000);
});


function setCookie(cname, cvalue, exmins) {
    var d = new Date();
    d.setTime(d.getTime() + (exmins*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function clearErrors(){
  $(".logInMsg").html("");
}

function verifyFields(){
  var email = $("#emailLogin").val();
  var pass = $("#passwordLogin").val();

  var emailErrorMessage="";
  var passwordErrorMessage="";

  if(email==""){
  	 emailErrorMessage = "This field is required.";
  }else{
  	var isEmail = /.+@.+\..+/
  	if(!isEmail.test(email)){
  		emailErrorMessage = "This field must be an email address."
  	}
  }
  $("#emailError").html(emailErrorMessage);

  if(pass==""){
  	passwordErrorMessage = "This field is required.";
  }
  $("#passError").html(passwordErrorMessage);

  if (emailErrorMessage=="" && passwordErrorMessage=="")
    return true;
  else
    return false;
}


function logIn(){
  var email = $("#emailLogin").val();
  var pass = $("#passwordLogin").val();


  $.ajax({
      type: "POST",
      url: "/login",
      data: {email:email,password:pass},
      success: function(data,status,xhr){

            if (xhr.status==302) {
                $("#generalErrors").html("Email or password invalid");
                return;
            }

            setCookie("token",xhr.getResponseHeader("authorization"),20);
            setCookie("user",JSON.stringify(data),20);

            setTimeout(function(){ 
                if($("#logInButton")[0].hasAttribute("data-firstTime"))
                  window.location.replace("/home?firstTime=true");
                else
                  window.location.replace("/home");
            }, 1000);

      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
         $("#generalErrors").html("Email or password invalid");
      }
  });

}

function signUp(){
  var email = $("#emailLogin").val();
  var pass = $("#passwordLogin").val();

  $("#emailLogin").val("");
  $("#passwordLogin").val("");

  $.ajax({
      type: "POST",
      url: "/signup",
      data: {email:email,password:pass},
      success: function(msg){
          $("#generalInfo").html("A confirmation email has been sent, please check your email");
          $("#infoMessage").html("");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          if(XMLHttpRequest.status == 409)
            $("#generalErrors").html("That user already exists");
          if(XMLHttpRequest.status == 500)
            $("#generalErrors").html("There was error, plase register again");
          if(XMLHttpRequest.status == 400)
            $("#generalErrors").html("Please make sure the email is correct");
          $("#infoMessage").html("");
      }
  });

}

function verifyEmailToRecover(){
  var email = $("#emailToRecover").val();

  var emailErrorMessage="";
  var passwordErrorMessage="";

  if(email==""){
     emailErrorMessage = "This field is required.";
  }else{
    var isEmail = /.+@.+\..+/
    if(!isEmail.test(email)){
      emailErrorMessage = "This field must be an email address."
    }
  }

  if (emailErrorMessage=="")
    return true;

  $("#emailError").html(emailErrorMessage);
  return false;
}

function recoverPassword(){
  var email = $("#emailToRecover").val();
  var fieldsErased=false;

  $.ajax({
      type: "POST",
      url: "/login/recover",
      data: {email:email},
      success: function(data,status,xhr){
              $(".row").fadeOut(500,function(){
                if(fieldsErased){
                  $(this).parent().append('<div id="messageRecoverPassword" class="row">A new password has been sent. <br> Please, check your email.</div>').hide().fadeIn(1000);;
                  setTimeout(function(){window.location.replace("/");}, 4000)
                }else
                  fieldsErased=true;
              });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
         $("#emailError").html("This email is not registered");
      }
  });
}


$("#logInButton").click(function(event){
  event.preventDefault();
  clearErrors();

  if(verifyFields())
    logIn();
});


$("#recoverButton").click(function(event){
  event.preventDefault();
  clearErrors();

  if(verifyEmailToRecover())
    recoverPassword();
});

$("#signUpButton").click(function(event){
  event.preventDefault();
  clearErrors();

  if(verifyFields())
    signUp();
});