var redis = require('redis');
var moment = require('moment'); 

if(process.env.REDISTOGO_URL){
	var rtg = require("url").parse(process.env.REDISTOGO_URL);
	var client = redis.createClient(rtg.port,rtg.hostname);
	client.auth(rtg.auth.split(":")[1]);
}else{
    var client = redis.createClient();
	client.select(1);
}

var dbAccess = {

	flushDB: function(){
		client.flushdb();
	},
	
	getUserById: function(id,success,fail){

		client.hgetall('user:'+id, function(err, user) {

					if(err || !user){
						fail();
					}else{
						success(user);
					}
				});
	},

	getUserByEmail: function(email,success,fail){

		client.hget('users',email, function(err, userId) {

					if(err || !userId){
						fail();
					}else{
						client.hgetall('user:'+userId, function(err2, user) {
							if(err2 || !user){
								fail();
							}else{
								success(user);
							}
						});
					}
				});
	},

	getUserId: function(email,success,fail){

		client.hget('users',email, function(err, userId) {

			if(err || !userId){
				fail();
			}else{
				success(userId);
			}
		});
	},

	createNewUser: function(newUser,success,fail){

		client.hget('users',newUser.email,function(err,id){

		    if(id || err){
		      	fail();
		    }else{
			    client.incr('nextUser',function(err2,userId){
			    	
			    	var user = newUser;
			        user.id=userId;

			        client.hmset('user:'+userId,user,function(err3,obj){    //Hash to find by userId

		         	  client.hset('users',user.email,userId,function(err4){  //Hash to find by email
		  			    success(user);	
		        	  });

			        });
			    });
		    }

		});
	},

	updateUser: function(id,newUserInfo,success,fail){

		client.hgetall('user:'+id, function(err, user) {
			if(err || !user){
				fail(404,'No user found');
			}else{

		    	var userInfo = updateUserInfo(newUserInfo, user);

				client.hmset('user:'+id,userInfo, function(err,obj){
					if(err){
					  fail(404,'Error when updating info in DB');
					}else{
					  userInfo.id = id;
					  success(userInfo);
					}				
				});
			}
		});
	},

	deleteUser: function(id,success,fail){

		this.getUserById(id,
			function(user){

				client.hdel('users',user.email, function(err) {
					if(err){
						fail(500,'Error deleting');
					}else{
						client.del('user:'+id, function(err2){
							if(err2)
							  fail(500,'Error deleting');
							else
							  success();
						});
					}
				});

			},
			function(){
				fail(404,'No user found');
			}
		);
	},

	addContact: function(id, newContactID, success, fail){

		//Check new contact exists
		client.hgetall('user:'+newContactID, function(err, user){
			if(err||!user){
				fail(404,'Contact not found');
			}else{
				//Get current user
				client.hgetall('user:'+id, function(err, user) {
					if(err || !user){
						fail(404,'User not found');
					}else{
						var stringJSON = '{"id":'+String(newContactID)+',"date":'+moment().format("YYYYMMDD")+'}';
						client.sadd('contacts:'+id,
							        stringJSON,
							        function(err,result){
										if(err)
											fail(404,'Error adding new contact:'+err);
										else{
											success(JSON.parse(stringJSON));
										}
						});
					}
				});
			}
		});
	},

	getContacts: function(id, success, fail){
		client.smembers('contacts:'+id,function(err,contacts){
			if(!err){
				var contactsAsObjects = contacts.map(function(stringJSON){return JSON.parse(stringJSON);});
				success(contactsAsObjects);
			}else{
				fail();
			}
		});
	},

	setTempVariable: function(id, value, ttlInSeconds, success, fail){
    	client.set(id+"-temp",value,function(err,reply){
    		if(reply){
    			client.expire(id+"-temp",ttlInSeconds);
				success();
			}else{
				fail();
			}
    	});
	},

	getTempVariable: function(id, success, fail){
    	client.get(id+"-temp",function(err,reply){
    		if(reply){
				success(reply);
			}else{
				fail();
			}
    	});
	},

	removeTempVariable: function(id){
    	client.del(id+"-temp",function(err){});
	}

}


//Private helpers

var updateUserInfo = function (newInfo, user){
	return {'password': 	newInfo.password   || user.password,
	 	    'email': 	    newInfo.email      || user.email,
	 	    'status': 		newInfo.status 	   || user.status,
	 	    'lastTested':   newInfo.lastTested || user.lastTested};
}





module.exports = dbAccess;
