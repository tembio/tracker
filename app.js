var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var hogan = require("hogan.js");
var fs = require('fs');
var urlencode = bodyParser.urlencoded({extended:false});

app.use(express.static(__dirname + '/static'));

app.use(urlencode);

//Routes
var users = require('./routes/users.js');
app.use('/users',users);

var login = require('./routes/login.js');
app.use('/login',login);

var signup = require('./routes/signup.js');
app.use('/signup',signup);

app.get('/home', function(req, res){
  res.setHeader('content-type', 'text/html');
  fs.readFile('views/index.html', 'utf8', function (err,data) {
  	if (err) 
  	  res.sendResponse(500);
  	else{

    	  if(req.query.firstTime == 'true'){
  		    res.status(200).send(hogan.compile(data).render({firstTime:true}));
  	    }else
  		    res.status(200).send(hogan.compile(data).render({firstTime:false}));
    }
  });

});

app.get('/', function(req, res){
  fs.readFile('views/init.html', 'utf8', function (err,data) {
  	if (err) 
	  res.sendResponse(500);
	else
	  res.status(200).end(hogan.compile(data).render({signUpResult:""}));
  });  
});

app.get('/recover', function(req, res){
  res.setHeader('content-type', 'text/html');
  res.sendfile('views/recover.html');
});


module.exports =  app;

