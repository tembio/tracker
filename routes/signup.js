var express = require('express');
var router = express.Router();
var db = require('../lib/database.js');
var avertMailer = require('../lib/mailer.js');
var hogan = require("hogan.js");
var fs = require('fs');
var encrypt = require('../lib/encrypt.js');
var text = require('../text.js');

router.route('/')
	.post(
		function(request,response){

			var userExists = function(user){
				response.sendStatus(409);
				return;
			};

			var userDoesNotExist = function(){


				db.getUserByEmail(request.body.email,userExists,function(){

						var encodedEmail =  encodeURIComponent(new Buffer(request.body.email).toString('base64'));
						
						//console.log(encodedEmail);

						var registrationLink = 'https://avert.herokuapp.com/signup/confirm?e='+ encodedEmail;

						var tempVarSuccess = function(){
						  	  fs.readFile('views/confirmationEmail.html', 'utf8', function (err,confirmationEmail) {
						  	  	if (err){ 
								 	db.removeTempVariable(request.body.email);
								 	response.sendStatus(500);
							  	}else{

									avertMailer.sendHTMLEmail(request.body.email, text.confirmationEmailTitle, 
															  hogan.compile(confirmationEmail).render({'registrationLink': registrationLink}),
															  text.confirmationEmailTextBody+registrationLink);	
									response.sendStatus(200);
							    }
						  	  });
						  };

						var tempVarFail = function(){
							response.sendStatus(409);
						  };

						db.setTempVariable(request.body.email, encrypt.getInstance().hash(request.body.password), 86400, tempVarSuccess, tempVarFail);
				});
				
			};

		  	if(!/.+@.+\..+/.test(request.body.email)){
				response.sendStatus(400);
  				return;
  			}  			

  			db.getTempVariable(request.body.email, userExists, userDoesNotExist);

		}
	);

router.route('/confirm')
	.get(
		function(request,response){

			var recievedEmail = decodeURIComponent(new Buffer(request.query.e, 'base64'));

			db.getTempVariable(recievedEmail, 
				function (password) {
					var newUser  = {'password':password,'email':recievedEmail,'status':'neg','lastTested':''};
					
					var success = function(user){
						sendSignUpResponse('validConfirmation.html',response);
						db.removeTempVariable(recievedEmail);
					};
					var fail = function(){
						sendSignUpResponse('errorCreatingUser.html',response);
						db.removeTempVariable(recievedEmail);
					};

					db.createNewUser(newUser,success,fail)
        		},
        		function(){
					sendSignUpResponse('linkExpired.html',response);
        		}
    		);
		}
	);


function sendSignUpResponse(fileName, response){
	fs.readFile('views/init.html', 'utf8', function (err,data) {
  	if (err) 
	  response.sendResponse(500);
	else
  	  fs.readFile('views/'+fileName, 'utf8', function (err2,signupMessage) {
  	  	if (err) 
	  	  response.sendResponse(500);
	  	else{
	  	  response.setHeader('content-type', 'text/html');
	      response.status(200).send(hogan.compile(data).render({signUpResult: signupMessage,firstTimeAttribute:'data-firstTime'}));
	    }
  	  });
  	});
}



//Exports to test
module.exports = router;