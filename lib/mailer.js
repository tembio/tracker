var express = require('express');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var config = require('../config.txt');

var options = {
    service: config.emailService,
    auth: {
        user: config.emailAccount,
        pass: config.emailPassword
    }
};

var transporter = nodemailer.createTransport(
    smtpTransport(options)
);


var mailer = {
	sendHTMLEmail : function(listOfRecipients, emailSubject, emailBody, bodyPlainText){

		var mailOptions = {
		    from: 'Avert App<info@avertapp.com>', // sender address 
		    to: listOfRecipients, // list of receivers 
		    subject: emailSubject, // Subject line 
		    text: bodyPlainText, 
		    html: emailBody // html body 
		};
		 
		// send mail with defined transport object 
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        return console.log(error);
		    }else{
		    	console.log('Message sent: ' + info.message);
			}
		});
	}
}


//Exports to test
module.exports = mailer;